package com.dryldmytro;

public class AliceParty {
    public int count = 0;
    public int countAllHearRumor = 0;

    public void rumorOnAliceParty(boolean[] peopleOnParty) {
        int randomNextPeople = 1;
        int previousGuest = 0;
        for (int i = 0; i < peopleOnParty.length; i++) {
            int rumorRandom = 1 + (int) (Math.random() * (peopleOnParty.length - 1));
            while (rumorRandom == randomNextPeople || rumorRandom == previousGuest) {
                rumorRandom = 1 + (int) (Math.random() * (peopleOnParty.length - 1));
            }
            if (peopleOnParty[rumorRandom]) {
                count = 0;
                countAllHearRumor = 0;
                break;
            } else peopleOnParty[rumorRandom] = true;
            previousGuest = randomNextPeople;
            randomNextPeople = rumorRandom;
        }
        for (int i = 0; i < peopleOnParty.length; i++) {
            if (peopleOnParty[i]) {
                count++;
            }
        }
        if (count == peopleOnParty.length) {
            countAllHearRumor++;
        }
    }
}

