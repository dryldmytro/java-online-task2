package com.dryldmytro;

public class AttemptsParty {
    static void getAttemptsParty(boolean[] peopleOnParty) {
        int attempts = 100;
        double howManyPersonHear = 0;
        double counAllHear = 0;
        AliceParty aliceParty = new AliceParty();
        for (int i = 0; i < attempts; i++) {
            aliceParty.rumorOnAliceParty(peopleOnParty);
            for (int j = 0; j < peopleOnParty.length; j++) {
                peopleOnParty[j] = false;
                peopleOnParty[0] = true;
                peopleOnParty[1] = true;
            }
            howManyPersonHear = howManyPersonHear + aliceParty.count;
            counAllHear = counAllHear + aliceParty.countAllHearRumor;
        }
        String averageAmount = "Average amount of people that rumor reached is: ";
        String chanceAllHear = "Chance what all will hear a rumor in ";
        System.out.println(averageAmount + howManyPersonHear / attempts + " with " + peopleOnParty.length);
        System.out.println(chanceAllHear + attempts + " attempts is: " + counAllHear / attempts * 100 + "%");
    }
}
