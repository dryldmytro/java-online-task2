package com.dryldmytro;

import java.util.Scanner;

public class NumberOfGuests {
    boolean[] getNumberOfGuests() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of guests(minimum 4): ");
        int numberOfGuests = scanner.nextInt();
        while (numberOfGuests <= 3) {
            System.out.println("Incorrect amount. Try again: ");
            numberOfGuests = scanner.nextInt();
        }
        boolean[] peopleOnParty = new boolean[2 + numberOfGuests];
        boolean Alica = true, Bob = true;
        peopleOnParty[0] = Alica;
        peopleOnParty[1] = Bob;
        return peopleOnParty;
    }
}